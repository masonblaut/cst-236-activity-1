<?php
//Mason Blaut
//This is my own work

class Person
{
    private $name;
    public $age;
    
    public function __construct($name)
    {
        $this->name = $name;
        $this->age = 10;
    }
    
    public function printName()
    {
        echo "My name is " . $this->name . " and this method is working.<br>";
    }
    
    
    public function walk()
    {
        echo "I am walking...<br>";
    }
    
    final public function growOlderBy($year)
    {
        $this->age = $this->age + $year;
    }
}

// Final Keyword Example

class Student extends Person
{
    public function growOlderBy($decade)
    {
        $this->age = $this->age + 10 * $decade;
    }
}