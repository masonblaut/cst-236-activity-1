<?php
require "personClass.php";

$newGuy1 = new Person("Jim");
$newGuy2 = new Student("Alex");

$newGuy1->growOlderBy(2);
$newGuy2->growOlderBy(2);

echo "The age of Person 1 ". $newGuy1->age . "<br/>";
echo "The age of Person 2 ". $newGuy2->age . "<br/>";