<?php
//Mason Blaut
//This is my own work

require "personClass.php";
require "carClass.php";
require "SuperHero.php";


$mason = new Person("Mason");

$mason->printName();
$mason->walk();

$car = new Car("Mustang", 5.0);


$car->printModel();
$car->printEngine();
$car->printTires();
$car->fillTires();
$car->printTires();
$car->startEngine();

echo "<br/>";
$Hero1 = new SuperHero("hero1", 50.0);
$Hero2 = new SuperHero("hero2", 70.0);

$Hero1->Attack($Hero2);

echo "<br/>";
$batman = new Batman();
$batman->Attack($Hero1);
$Hero1->Attack($batman);