<?php

abstract class Animal
{
    public $name;
    public $color;
    
    public function __construct ($n, $c)
    {
        $this->name = $n;
        $this->color = $c;
    }
    
    abstract function talk();
    abstract function doTrick();
}

class Dog extends Animal
{
    public function talk()
    {
        echo "bark <br/>";
    }
    
    public function doTrick()
    {
        echo "Dog fetches<br/>";
    }
}

class Cat extends Animal
{
    public function talk()
    {
        echo "meow <br/>";
    }
    public function doTrick()
    {
        echo "Cat sleeps <br/>";
    }
}


$dog1 = new Dog("Borker", "Black");
$cat1 = new Cat("Mo", "Orange");

$dog1->talk();
$cat1->talk();

$dog1->doTrick();
$cat1->doTrick();
