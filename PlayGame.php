<?php
//Mason Blaut
//This is my own work

//Contains both Batman and Superman classes
require "SuperHero.php";

$batman = new Batman();
$superman = new Superman();

while ($batman->isDead != true && $superman->isDead!= true)
{
    $batman->Attack($superman);
    $superman->Attack($batman);
}