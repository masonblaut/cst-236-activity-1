<?php
//Mason Blaut
//This is my own work

class   Car
{
    private $model;
    private $tires;
    private $engineSize;
    private $engine;
    
    public function __construct($model, $engineSize)
    {
        $this->model = $model;
        $this->engineSize = $engineSize;
        $this->engine = new Engine(FALSE);
        $this->tires = new Tires();
        
    }
    
    public function printModel()
    {
        echo "Model: " . $this->model." <br/>";
    }
    
    public function printTires()
    {
        echo "Tires: ".$this->tires->getPsi()." psi.<br/>";
    }
    
    public function fillTires()
    {
        $this->tires->setPsi(40);
        echo "Filling the tires!<br/>";
    }
    
    public function startEngine()
    {
        $this->engine->setIsRunning(TRUE);
        echo "The engine is started!";
    }
    
    public function printEngine()
    {
        echo "Engine: ".$this->engineSize." liters <br/>";
        if ($this->engine->getIsRunning())
        {
            echo "The engine is running!<br/>";
        }
        else
        {
            echo "The engine is off.<br/>";
        }
    }
}

class Engine
{
    private $isRunning;
    
    public function __construct($isRunning)
    {
        $this->isRunning = $isRunning;
    }
    
    public function getIsRunning()
    {
        return $this->isRunning;
    }
    
    public function setIsRunning($running)
    {
        $this->isRunning = $running;
    }
    
}

class Tires
{
    private $psi;
    
    public function __construct()
    {
      $this->psi = 0;
    }
    
    public function setPsi($newPsi)
    {
        $this->psi = $newPsi;
    }
    
    public function getPsi()
    {
        return $this->psi;
    }
}