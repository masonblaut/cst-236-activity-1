<?php

class SuperHero
{
    public $name;
    public $health;
    public $isDead;
    
    public function __construct($name, $health)
    {
        $this->name = $name;
        $this->health = $health;
        $this->isDead = false;
        
    }
    
    public function Attack($otherHero)
    {
        
        $damage = rand(1, 10);
        $this->DetermineHealth($damage, $otherHero);
        
    }
    
    private function DetermineHealth($dmg, $otherHero)
    {
        $otherHero->health = $otherHero->health - $dmg;
        echo "" . $this->name . " did " . $dmg . " to " . $otherHero->getName() . ", who now has "
            . $otherHero->health . " health.<br/>";
        if ($otherHero->health <= 0)
        {
            $otherHero->isDead = true;
            echo "". $otherHero->getName() . " is dead! <br/>";
        }
    }
    
    private function isDead()
    {
        return $this->isDead;
    }
    
    private function getName()
    {
        return $this->name;
    }
}

class Batman extends SuperHero
{
    
    public function __construct()
    {
        $this->name = "Batman";
        $this->health = rand(1, 1000.0);
        $this->isDead = false;
        
    }
}

class Superman extends SuperHero
{
    
    public function __construct()
    {
        $this->name = "Superman";
        $this->health = rand(1, 1000.0);
        $this->isDead = false;
        
    }
}